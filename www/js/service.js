angular.module('starter.services', [])

.factory('service', function($http, $q, $window) {
  return {
    get: function(url) {
      var deferred = $q.defer();
      $http.get(url)
        .success(function(data) {
          deferred.resolve(data);
        }).error(function(msg, code) {
          deferred.reject(code);
        });
      return deferred.promise;
    },
    post: function(url, obj) {
      var deferred = $q.defer();
      $http.post(url, obj)
        .success(function(data) {
          deferred.resolve(data);
        }).error(function(msg, code) {
          deferred.reject(code);
        });
      return deferred.promise;
    }
  };
});